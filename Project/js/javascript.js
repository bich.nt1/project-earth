function mainpage() {
    var windowWidth = jQuery(window).width();
    var widthContainer = $('.container').width();
    var windowHeight = $(window).height();
    var heightHeader = $('header').height();
    var heightFooter = $('footer').height();
    $(".main-page").css("min-height", windowHeight - heightHeader - heightFooter);
}

$(document).ready(function () {
    $('header .navbar-toggle').click(function () {
        $('body').toggleClass('overflow');
    });

    $(window).scroll(function () {
        var sticky = $('.header'),
            scroll = $(window).scrollTop();

        if (scroll > 0) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
    });


    // $('.chart-item').click(function(){
    //     var t = $(this).attr('id');
    //
    //     if($(this).hasClass('active')){ //this is the start of our condition
    //         $('.chart-item').addClass('active');
    //         $(this).removeClass('active');
    //         $('.roadmap-content').hide();
    //         $('#'+ t + 'C').fadeIn('slow');
    //     }
    // });

    // $(".chart-item").click(function(){
    //     $(this).tab('show');
    // });

    // $('.datepicker').datepicker();

    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top-200
            }, 500);
            return false;
          }
        }
    });

    $(window).scroll(function () {
        $(window).scrollTop() > 300 ? $(".go_top").addClass("go_tops") : $(".go_top").removeClass("go_tops")
    });

    $('#teams').click(function () {
        setTimeout(() => {
            $('.slick')
            .on('init', () => {
                $('.slick-slide[data-slick-index="-2"]').addClass('lt2');
                $('.slick-slide[data-slick-index="-1"]').addClass('lt1');
                $('.slick-slide[data-slick-index="1"]').addClass('gt1');
                $('.slick-slide[data-slick-index="2"]').addClass('gt2');
            })
            .slick({
                centerMode: true,
                centerPadding: 0,
                slidesToShow: 5
            }).on('beforeChange', (event, slick, current, next) => {
                $('.slick-slide.gt2').removeClass('gt2');
                $('.slick-slide.gt1').removeClass('gt1');
                $('.slick-slide.lt1').removeClass('lt1');
                $('.slick-slide.lt2').removeClass('lt2');
    
                const lt2 = (current < next && current > 0) ? current - 1 : next - 2;
                const lt1 = (current < next && current > 0) ? current : next - 1;
                const gt1 = (current < next || next === 0) ? next + 1 : current;
                const gt2 = (current < next || next === 0) ? next + 2 : current + 1;
    
                $(`.slick-slide[data-slick-index="${lt2}"]`).addClass('lt2');
                $(`.slick-slide[data-slick-index="${lt1}"]`).addClass('lt1');
                $(`.slick-slide[data-slick-index="${gt1}"]`).addClass('gt1');
                $(`.slick-slide[data-slick-index="${gt2}"]`).addClass('gt2');
    
                // Clone processing when moving from 5 to 0
                if (current === 5 && next === 0) {
                    $(`.slick-slide[data-slick-index="${current - 1}"]`).addClass('lt2');
                    $(`.slick-slide[data-slick-index="${current}"]`).addClass('lt1');
                    $(`.slick-slide[data-slick-index="${current + 2}"]`).addClass('gt1');
                    $(`.slick-slide[data-slick-index="${current + 3}"]`).addClass('gt2');
                }
    
                // Clone processing when moving from 0 to 5
                if (current === 0 && next === 5) {
                    $(`.slick-slide[data-slick-index="${current - 1}"]`).addClass('gt2');
                    $(`.slick-slide[data-slick-index="${current}"]`).addClass('gt1');
                    $(`.slick-slide[data-slick-index="${current - 2}"]`).addClass('lt1');
                    $(`.slick-slide[data-slick-index="${current - 3}"]`).addClass('lt2');
                }
    
            });
        }, 300);
    });

    mainpage();
    jQuery(window).resize(function () {
        mainpage();
    });

});


